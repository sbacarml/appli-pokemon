package vue;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

//Import pour menu
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
//

import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

import static modele.Menu.ITEMS_MENU_PLANNING;

public class Fenetre extends Application {

    public void start(Stage stage) throws IOException {
        VBoxMenu root = new VBoxMenu();
        File css = new File("CSS"+ File.separator+"CSS.css");
        Scene scene = new Scene(root, 400, 380);
        scene.getStylesheets().add(css.toURI().toString());
        stage.setScene(scene);
        stage.setTitle("APLI");
        stage.setWidth(600);
        stage.setHeight(700);
        stage.centerOnScreen();
        stage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
