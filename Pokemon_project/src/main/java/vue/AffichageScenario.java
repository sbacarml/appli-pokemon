package vue;

import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import modele.Scenario;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.TreeMap;
import static outil.LectureEcriture.lectureScenario;


public class AffichageScenario extends HBox {
    private static Scenario monScenario = new Scenario();
    private static Scenario monScenario2 = new Scenario();

    public AffichageScenario() throws IOException {
        super(20);

        ////////////////////////////////////////Partie Scenario.txt////////////////////////////////////////
        //monScenario = lectureScenario(new File("Ressources" + File.separator + "scenario_2_2.txt"));                  //lecture d'un scenario depuis le fichier Ressources
        monScenario = lectureScenario(new File("Ressources" + File.separator + "scenario_2_2.txt")); //lecture d'un scenario depuis le fichier Ressources
        Label labScenario = new Label(monScenario.toString());                                  //création du label qui servira à afficher les scénario graphiquement

        /*ecritureScenario("Ressources"+ File.separator+ "test.txt",new Scenario());            //création d'un nouveau scenario "test.txt." dans le fichier Ressources
        monScenario2 = lectureScenario(new File("Ressources" + File.separator + "test.txt"));
        Label labScenario2 = new Label(monScenario2.toString());*/

        HBox hbox = new HBox();
        hbox.getChildren().add(labScenario);
        //hbox.getChildren().add(labScenario2);
        ScrollPane scrollpane = new ScrollPane();
        scrollpane.setContent(hbox);
        this.getChildren().addAll(scrollpane);
        //this.getChildren().add(scrollpane);
        //this.getChildren().addAll(labScenario);
        //this.getChildren().addAll(labScenario2);
    }
    //////////////////////////////////////////////////////////////////////////////////////////////


    public void main(String[] args) throws IOException {


    }
}

